import {
  Box,
  Flex,
  Input,
  Text,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Stack
} from "@chakra-ui/react"
import { ethers } from "ethers"
import { useState, useEffect } from "react"
import Button from '../components/Button'
import Auction from '../components/Auction'
import NewAuction from '../components/NewAuction'
import auction from "../abis/Auction.json"


const Auctions = ({ accounts, auctions, setAuctions }) => {
  const isConnected = Boolean(accounts[0])
  const [auctionToAdd, setAuctionToAdd] = useState('')
  const [networkName, setNetworkName] = useState('')
  const [inputError, setInputError] = useState('')

  useEffect(() => {
    const getNetworkName = async () => {
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const network = await provider.getNetwork()
      setNetworkName(network.name === 'unknown' ? 'localhost' : network.name)
    }

    getNetworkName()
  }, [])

  const tryAddAuction = async () => {
    console.log('auctions in auctions', auctions)
    if (auctions.find(auction => auction.address === auctionToAdd)) setInputError('already added')
    else {
      const contract = await getContract(auctionToAdd)

      try {
        const code = await contract.provider.getCode(auctionToAdd);

        if ((code === '0x')) {
          setInputError('contract not found')
          return
        }
        const newAuctions = [...auctions]
        newAuctions.push({ address: auctionToAdd })
        setAuctions(newAuctions)
        setInputError('')
      } catch (err) {
        setInputError('not the right type of contract')
        console.log("error: ", err)
      }
    }
  }

  const handleInput = (e) => {
    setAuctionToAdd(e?.target?.value ?? '')
    setInputError('')
  }

  const handleRemove = (auctionToRemove) => {
    const newAuctions = [...auctions].filter((auction) => {
      return auction.address !== auctionToRemove
    })

    setAuctions(newAuctions)
  }

  const getContract = async (address) => {
    if (window.ethereum) {
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const signer = provider.getSigner()

      const contract = await new ethers.Contract(
        address,
        auction.abi,
        signer
      )

      return contract
    }
  }

  const refresh = async () => {
    // const newAuctions = [...auctions]

    // newAuctions.forEach(auction => {
    //   delete auction.contract
    // })

    // setAuctions(newAuctions)
  }

  const updateContract = async (address, details = {}) => {
    const newAuctions = [...auctions]
    let index = 0
    if (newAuctions.find((auction, i) => {
      index = i
      return auction?.address?.toLowerCase() === address?.toLowerCase()
    })) {
      let updatedAuction = {
        ...auction,
        ...details,
        address
      }

      if (!auction.contract) {
        const contract = await getContract(address)

        const code = await contract.provider.getCode(address);

        if (code === '0x') {
          console.log('code bad', code)
          handleRemove(address) //didn't find example contract(s) on goerli
          return
        }

        updatedAuction.contract = contract
      }
      newAuctions[index] = { address, ...updatedAuction }
    }

    setAuctions(newAuctions)
  }

  return (
    <Flex justify="center" align="center" height="100%">
      <Box width="720px" height='100%' padding='60px 20px'>
        <div style={{ width: 'auto' }}>
          <Text fontSize="48px">
            French Toast Flavor Auction
          </Text>
          <Text>Network name: {networkName}</Text>
        </div>
        <form onSubmit={(e) => {
          e.preventDefault()
          tryAddAuction()
        }}>
          <FormControl isInvalid={inputError.length > 0}>
            <FormLabel>Auction Address</FormLabel>
            <Stack spacing={8} direction='row'>
              <Box width='100%'>
                <Input value={auctionToAdd}
                  placeholder='Auction to view'
                  onChange={(e) => handleInput(e)}
                />
                {inputError === '' ? (
                  <FormHelperText>
                    Add existing address to interact
                  </FormHelperText>
                ) : (
                  <FormErrorMessage>{inputError}</FormErrorMessage>
                )}
              </Box>
              <Button onClick={tryAddAuction}>Go</Button>
            </Stack>
          </FormControl>
        </form>
        <NewAuction account={accounts[0]} setAuctions={setAuctions} auctions={auctions} />
        {isConnected ? (
          <p>Connected to wallet {accounts[0]}!</p>
        ) : (
          <Text
            marginTop="70px"
            fontSize="30px"
            letterSpacing="5.5%"
            textShadow="0 3px #000000"
            color="#008fd4"
          >
            Connect your wallet to interact.
          </Text>
        )}

        {auctions.length ? (
          auctions.map(auction => {
            return (
              <Auction updateContract={updateContract} refresh={refresh} account={accounts[0]} contract={auction?.contract} key={auction?.address} auctionAddress={auction?.address} isConnected={isConnected}></Auction>
            )
          })
        ) : (
          <p>no length</p>
        )}

      </Box>
    </Flex>
  )
}

export default Auctions
