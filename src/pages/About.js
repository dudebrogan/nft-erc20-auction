import { Box, Button, Flex, Text } from '@chakra-ui/react'
import { useNavigate } from 'react-router-dom'

const About = () => {
  const navigate = useNavigate()

  return (
    <Flex justify="center" align="center" height="100vh" paddingBottom="150px">
      <Box width="520px">
        <div>
          <Text fontSize="48px">
            FrenchToastFlavors Auction
          </Text>
          <Text
            fontSize="18px"
            letterSpacing="-5.5%"
          >
            This application is for interaction with the Auction contract. For sale will be the greatest item of all, a coveted FrenchToastFlavors NFT.
          </Text>

          <Text
            fontSize="18px"
            letterSpacing="-5.5%"
          >
            Once the owner starts the auction, you can bid on it using FrenchToastken. You can withdraw your bid at any time. The highest bidder will receive their NFT once the auctioneer finalizes the auction.
          </Text>

          <Button
            backgroundColor="#008fd4"
            borderRadius="5px"
            boxShadow="0px 2px 2px 1px #0F0F0F"
            color="white"
            cursor="pointer"
            fontFamily="inherit"
            padding="15px"
            margin="10"
            onClick={() => navigate("/auction")}>
            Go to Auction
          </Button>
        </div>
      </Box>
    </Flex>
  )
}

export default About
