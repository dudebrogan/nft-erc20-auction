import './App.css'
import NavBar from './NavBar'
import Auctions from './pages/Auctions'
import About from './pages/About'
import { useEffect, useState } from 'react'

import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom"

import {
  ChakraProvider,
  extendTheme,
} from "@chakra-ui/react"

const theme = extendTheme({
  components: {
    Heading: {
      variants: {
        green: {
          color: "green.300"
        }
      }
    }
  }
})

function App() {
  const [accounts, setAccounts] = useState([])
  const [auctions, setAuctionsState] = useState([]);

  useEffect(() => {
    let auctions = localStorage.getItem('auctions')
    const stuff = JSON.parse(auctions) ?? []
    setAuctionsState(stuff.map(address => {return {address}}))
  }, [])
  const setAuctions = (auctions) => {
    localStorage.setItem('auctions', JSON.stringify(auctions.map(auction => auction.address)))
    setAuctionsState(auctions)
  }
  return (
    <Router>
      <div className="overlay">
        <div className="App">
          <ChakraProvider theme={theme}>
            <NavBar className='navbar' accounts={accounts} setAccounts={setAccounts} />
            <div className='border'>
              <Routes>
                <Route path="/auction" element={<Auctions accounts={accounts} setAccounts={setAccounts} auctions={auctions} setAuctions={setAuctions} />}>
                </Route>
                <Route path="/" element={<About />}>
                </Route>
              </Routes>
            </div>
          </ChakraProvider>
        </div>
      </div>
    </Router>
  )

}

export default App
