import { ethers } from "ethers"
import Button from './Button'
import Bidder from './Bidder'
import Owner from './Owner'
import React, { useState, useEffect } from "react"
import {
    Box,
    Text,
    VStack
} from '@chakra-ui/react'
import erc20 from "../abis/ERC20.json"
import erc721 from "../abis/ERC721.json"

const Auction = ({ refresh, account, contract, auctionAddress, isConnected, updateContract }) => {
    const [isOwner, setIsOwner] = useState(false)
    const [owner, setOwner] = useState('')
    const [showAuctionOver, setShowAuctionOver] = useState(false)
    const [displayName, setDisplayName] = useState('')
    const [highestBid, setHighestBid] = useState(0)
    const [tokenId, setTokenId] = useState(0)
    const [highestBidder, setHighestBidder] = useState('')
    const [auctionStatus, setAuctionStatus] = useState('')
    const [auctionEndTimestamp, setAuctionEndTimestamp] = useState('')
    const [auctionLengthInHours, setAuctionLengthInHours] = useState()
    const [currencyContract, setCurrencyContract] = useState(undefined)
    const [nftContract, setNFTContract] = useState(undefined)
    const [balance, setBalance] = useState('')

    useEffect(() => {
        getUserBalance()
        setTimeout(() => updateContract(auctionAddress, {}), 1500)
    }, [])

    const getUserBalance = async () => {
        try {
            if (account) {
                const _balance = await currencyContract?.balanceOf(account)
                if (_balance) setBalance(_balance.toString())
            }
        } catch (e) {
            console.error('user bal e', e)
        }
    }

    const refreshAuction = () => {
        updateContract(auctionAddress, { contract: undefined })
        refresh()
    }

    useEffect(() => {
        if (!contract) {
            updateContract(auctionAddress, {})
        } else {
            try {
                getAuctionDetails()
                getNFTDetails()
                getCurrencyDetails()
                getUserBalance()
            } catch (e) {
                console.error('get auction info error', e)
                updateContract(auctionAddress)
            }
        }
    }, [contract, account])

    const getCurrencyDetails = async () => {
        const currencyAddress = await contract.biddingCurrency()
        const provider = new ethers.providers.Web3Provider(window.ethereum)
        const signer = provider.getSigner()
        let _currencyContract = await new ethers.Contract(
            currencyAddress,
            erc20.abi,
            signer
        )
        setCurrencyContract(_currencyContract)
    }


    const getAuctionDetails = async () => {
        try {
            const owner = await contract.owner()

            setIsOwner(owner.toLowerCase() === account?.toLowerCase())
            setOwner(owner.toLowerCase())

            const bid = await contract.getHighestBidAmount()
            const bidder = await contract.getHighestBidder()
            const length = await contract.auctionLengthInHours()
            const status = await contract.getAuctionStatus()
            const endTime = await contract.auctionEndTime()

            setAuctionStatus(status ?? '')
            console.log('status', status)
            // setAuctionEndTime(endTime)
            setAuctionEndTimestamp(getDate(endTime * 1000))
            setAuctionLengthInHours(length.toNumber())
            setHighestBid(bid.toNumber())
            setHighestBidder(bidder === account ? 'you!' : bidder)
        } catch (e) {
            console.error('auction details error', e)
        }
    }

    const getDate = (endTime) => {
        const d = new Date(endTime)
        let s = d.toUTCString()
        s = s.substring(0, s.indexOf("GMT")) + "UTC"

        console.log('get date', s)
        return s
    }

    const getNFTDetails = async () => {
        try {
            const nftAddress = await contract.nftContract()
            const provider = new ethers.providers.Web3Provider(window.ethereum)
            const signer = provider.getSigner()
            let _nftContract = await new ethers.Contract(
                nftAddress,
                erc721.abi,
                signer
            )

            const name = await _nftContract.name()
            const tokenId = await contract.tokenId()
            const owner = await contract.owner()
            const endTime = await contract.auctionEndTime()

            if (auctionStatus && auctionStatus !== 'hold your horses' && auctionStatus !== '') {
                 setAuctionEndTimestamp(endTime * 1000)
            }
            const nftOwner = await _nftContract.ownerOf(tokenId)

            _nftContract = {
                ..._nftContract,
                name
            }

            setNFTContract(_nftContract)
            setTokenId(tokenId)

            if (nftOwner !== owner) setShowAuctionOver(true)

            setDisplayName(`${name} #${tokenId}`)
        } catch (e) {
            console.error('Get nft details error', e)
        }
    }


    return (
        <>
            {contract ? (
                <Box borderWidth='1px' borderRadius='lg' overflow='hidden' minH='300px'>
                    <VStack align={'flex-start'} margin={'10px 40px'}>
                        <Button onClick={() => refreshAuction()}>Refresh</Button>
                        <Text>Auction Address: {auctionAddress}</Text>
                        <Text>Token and id: {displayName}</Text>
                        {!isOwner && (<Text>Owner: {owner}</Text>)}
                        <Text>High bidder: {highestBidder}</Text>
                        <Text>High bid: {highestBid}</Text>
                        <Text>Balance: {balance}</Text>
                        <Text>End: {auctionEndTimestamp}</Text>
                        <Text>Auction length in hours: {auctionLengthInHours}</Text>
                    </VStack>
                    {showAuctionOver ? (
                        <p>Auction over</p>
                    ) : (
                        <>
                            {isConnected ? (
                                <Box width={'100%'} >
                                    {
                                        isOwner ? (
                                            <Owner updateContract={updateContract} tokenId={tokenId} auctionAddress={auctionAddress} contract={contract} account={account} auctionStatus={auctionStatus} nftContract={nftContract} currencyContract={currencyContract} />
                                        ) : (
                                            <Bidder tokenId={tokenId} auctionAddress={auctionAddress} contract={contract} account={account} auctionStatus={auctionStatus} nftContract={nftContract} currencyContract={currencyContract} updateContract={updateContract} />
                                        )
                                    }
                                </Box>
                            ) : (
                                <Text
                                    marginTop="70px"
                                    fontSize="30px"
                                    letterSpacing="5.5%"
                                    textShadow="0 3px #000000"
                                    color="#008fd4"
                                >
                                    Connect your wallet to interact.
                                </Text>
                            )}
                        </>
                    )}
                </Box>
            ) : (<></>)
            }
        </>
    )
}

export default Auction