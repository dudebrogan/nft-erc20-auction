import { useState, useEffect } from "react"
import { FormControl, FormHelperText, HStack, FormErrorMessage } from '@chakra-ui/react'
import Button from './Button'

const Owner = ({ updateContract, nftContract, contract, auctionStatus, tokenId, auctionAddress, currencyContract }) => {
    const [startError, setStartError] = useState('')
    const [approveError, setApproveError] = useState('')
    const [finalizeError, setFinalizeError] = useState('')

    useEffect(() => {
        setApproveError('')
        const checkApproval = async () => {
            await contract.auctionEndTime()
             if (!nftContract) return
            const approvedAddress = await nftContract.getApproved(tokenId)
            
            if (approvedAddress?.toLowerCase() !== auctionAddress?.toLowerCase()) {
                setApproveError('contract is not approved for nft')
            }
        }

        checkApproval()
    }, [nftContract])

    const startAuction = async () => {
        try {
            await contract.startAuction()
            updateContract(auctionAddress)
        } catch (e) {
            console.log('e', e)
            setStartError(e?.error?.data?.message.replace('VM Exception while processing transaction: reverted with reason string', '') ?? '')
        }
    }

    const finalize = async() => {
        try {
            await contract.finalizeAuction()
        } catch(e) {
            console.log('finalize e: ', e)
        }
    }

    const approveNft = async () => {
        try {
            await nftContract.approve(contract.address, tokenId.toNumber())

            updateContract()
        } catch (e) {
            console.log('e', e)
        }
    }

    const canFinalize = auctionStatus === 'too late scrub'
    const canStart = auctionStatus === 'hold your horses' && approveError.length === 0

    return (
        <>
            <p>I am the owner</p>
            <FormControl isInvalid={startError?.length > 0 || approveError?.length > 0}>
                <HStack>
                    <Button disabled={!canStart} onClick={() => startAuction()}>Start</Button>
                    {canStart && (
                        <FormHelperText minWidth={'100px'}>
                            Click now to start auction
                        </FormHelperText>
                    )}
                    {startError !== '' && (
                        <FormErrorMessage>
                            {startError}
                        </FormErrorMessage>
                    )}
                </HStack>
            </FormControl>
            <FormControl isInvalid={finalizeError.length > 0}>
                <HStack>
                    <Button disabled={!canFinalize} onClick={finalize}>Finalize</Button>
                    {canFinalize ? (
                        <FormHelperText minWidth={'100px'}>
                            Click now to finalize auction and get your cash
                        </FormHelperText>
                    ) : (
                        <FormHelperText minWidth={'100px'}>
                            cannot finalize yet because of this status: {auctionStatus}
                        </FormHelperText>
                    )}
                    {startError !== '' && (
                        <FormErrorMessage>
                            {startError}
                        </FormErrorMessage>
                    )}
                </HStack>
            </FormControl>
            <FormControl isInvalid={approveError.length > 0}>
                <HStack>
                    <Button disabled={!approveError} onClick={() => approveNft()}>Approve NFT for auction</Button>
                    {approveError && (
                        <FormErrorMessage>
                            {approveError}
                        </FormErrorMessage>
                    )}
                </HStack>
            </FormControl>
        </>
    )
}

export default Owner