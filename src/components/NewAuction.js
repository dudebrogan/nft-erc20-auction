import Button from './Button'
import { ethers, Wallet } from "ethers"
import erc20 from "../abis/ERC20.json"
import erc721 from "../abis/ERC721.json"
import auctionOutput  from "../abis/Auction.json"

const NewAuction = ({ account, setAuctions, auctions }) => {
  const deployNewAuction = async () => {
    console.log('fart')
    const utils = ethers.utils
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const signer = await provider.getSigner(account)

    const Nft = new ethers.ContractFactory(erc721.abi, erc721.bytecode, '')
    const nft = await Nft.connect(signer).deploy()

    const Token = new ethers.ContractFactory(erc20.abi, erc20.bytecode, '')
    const token = await Token.connect(signer).deploy()

    await nft.deployed()
    await token.deployed()

    const tx = await nft.connect(signer).safeMint(signer._address)
    tx.wait()

    await token.connect(signer).transfer('0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266', utils.parseEther('10000'))
    await token.connect(signer).transfer('0x70997970C51812dc3A010C7d01b50e0d17dc79C8', utils.parseEther('10000'))
    await token.connect(signer).transfer('0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC', utils.parseEther('10000'))
    await token.connect(signer).transfer('0x90F79bf6EB2c4f870365E785982E1f101E93b906', utils.parseEther('10000'))

    const Auction = new ethers.ContractFactory(auctionOutput.abi, auctionOutput.bytecode, '')
    const auction = await Auction.connect(signer).deploy(nft.address, 0, token.address, 12)

    await auction.deployed()

    const newAuctions = [...auctions]

    newAuctions.push({address: auction.address})
    setAuctions(newAuctions)
  }

  return (
    <Button disabled={account === '' || account === undefined} onClick={deployNewAuction}>Create new auction</Button>
  )
}

export default NewAuction