import { Button } from "@chakra-ui/react";

const FTButton = ({onClick, children, ...restProps}) => {
    return(
        <Button
            backgroundColor="#008fd4"
            borderRadius="5px"
            boxShadow="0px 2px 2px 1px #0F0F0F"
            color="white"
            cursor="pointer"
            fontFamily="inherit"
            padding="15px"
            margin="10"
            onClick={onClick}
            {...restProps}
        >{children}</Button>
    )
}

export default FTButton