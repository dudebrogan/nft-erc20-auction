import { HStack, Box, Input, FormHelperText, FormControl, FormErrorMessage } from '@chakra-ui/react'
import Button from './Button'
import { useEffect, useState } from "react"
import { ethers } from "ethers"

const Bidder = ({ account, contract, auctionStatus, nftContract, currencyContract, updateContract }) => {
    const [bidAmount, setBidAmount] = useState(0)
    const [approveAmount, setApproveAmount] = useState(0)
    const [existingBid, setExistingBid] = useState(0)
    // const [bidAmount, setBidAmount] = useState('')
    const [inputError, setInputError] = useState('')
    const [approvalError, setApprovalError] = useState('')
    const [withdrawError, seWithdrawError] = useState('')

    useEffect(() => {
        if (account && contract) getBidDetails()
    }, [account, contract])

    const getBidDetails = async () => {
        try {
            const userBid = await contract?.bidAmounts(account)
            if(userBid) setExistingBid(userBid.toNumber())
        } catch (e) {
            console.log('e1', e)
            setInputError(e?.error?.data?.message.replace('VM Exception while processing transaction: reverted with reason string', '') ?? '')
        }

    }

    const bid = async () => {
        try {
            setInputError('')
            await contract.bid(bidAmount)
            setExistingBid(bidAmount)
            // updateContract(contract.address, {}, true)
        } catch (e) {
            console.log('e', e)
            setInputError(e?.error?.data?.message.replace('VM Exception while processing transaction: reverted with reason string', '') ?? '')
        }
    }

    const withdraw = async() => {
        try {
            await contract.withdrawBid()
        } catch(e) {
            console.log('withdraw e', e)
        }
    }

    const approveErc20 = async () => {
            const utils = ethers.utils

        if (approveAmount <= 0) setApprovalError('must be higher')
        try {
            await currencyContract.approve(contract.address, approveAmount)
        } catch(e) {
            console.log('approve erc20 error, ', e)
        }
    }

    const auctionActive = auctionStatus === 'it is on like donkey kong right now, bid your life savings away'

    const hasInputError = inputError?.length > 0
    const hasApproveError = approvalError?.length > 0

    const HelperText = existingBid > 0 ? `Current Bid: ${existingBid}` : `Bid now!`
    return (
        <Box width={'100%'}>
            <FormControl isInvalid={hasApproveError}>
                <HStack>
                    <Button disabled={!auctionActive} onClick={() => approveErc20()}>Approve ERC20 for contract</Button>
                    <HStack>
                        <Input style={{ margin: '0px 10px 0px 0px' }} value={approveAmount} type={'number'} onChange={(e) => setApproveAmount(e.target.value)} />
                        {auctionActive && (
                            <>
                                <FormHelperText minWidth={'100px'}>
                                    {HelperText}
                                </FormHelperText>
                                {hasInputError && (
                                    <FormErrorMessage>{inputError}</FormErrorMessage>
                                )}
                            </>
                        )}
                    </HStack>
                </HStack>
            </FormControl>
            <FormControl isInvalid={hasInputError}>
                <HStack>
                    <Button disabled={!auctionActive} onClick={() => bid()}>Bid</Button>
                    <HStack>
                        <Input style={{ margin: '0px 10px 0px 0px' }} value={bidAmount} type={'number'} onChange={(e) => setBidAmount(e.target.value)} />
                        {auctionActive && (
                            <>
                                <FormHelperText minWidth={'100px'}>
                                    {HelperText}
                                </FormHelperText>
                                {hasInputError && (
                                    <FormErrorMessage>{inputError}</FormErrorMessage>
                                )}
                            </>
                        )}
                    </HStack>
                </HStack>
            </FormControl>
            <FormControl isInvalid={withdrawError.length > 0}>
                <HStack>
                    <Button disabled={existingBid === 0 || !auctionActive} onClick={withdraw}>Withdraw Bid</Button>
                </HStack>
            </FormControl>
        </Box >
    )
}

export default Bidder
