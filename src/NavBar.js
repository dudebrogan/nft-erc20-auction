import React from 'react';
import { Box, Button, Flex, Link, Spacer } from '@chakra-ui/react';
import { Link as ReachLink } from "react-router-dom"

const NavBar = ({ accounts, setAccounts }) => {
    const isConnected = Boolean(accounts[0]);

    window.ethereum.on('accountsChanged', (accounts) => setAccounts(accounts))
    window.ethereum.on('chainChanged', () => window.location.reload())

    async function connectAccount() {
        if (window.ethereum) {
            const accounts = await window.ethereum.request({
                method: "eth_requestAccounts",
            });
            setAccounts(accounts);
        }
    }

    return (
        <Flex  color={'black'} justify="flex-end" align="center" padding="1px" background={'#bfe2ff'} >
            <Flex justify="space-between" align="center" width="40%" padding="10px">
                <Link as={ReachLink} to='/'>About</Link>
                <Spacer />
                <Link as={ReachLink} to='/auction'>Auction</Link>
                <Spacer />

                {isConnected ? (
                    <Box margin="0 15px">Connected</Box>
                ) : (
                    <Button
                        backgroundColor="#261300"
                        borderRadius="5px"
                        boxShadow="0px 2px 2px 1px #0f0f0f"
                        color="#ffffff"
                        cursor="pointer"
                        fontFamily="inherit"
                        padding="15px"
                        margin="0 15px"
                        onClick={connectAccount}
                    >
                        Connect
                    </Button>
                )}
            </Flex>

        </Flex>
    )
};

export default NavBar;
