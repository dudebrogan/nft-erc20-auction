// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const hre = require("hardhat")
const { ethers } = hre

async function main() {
    const [auctioneer, tokenDeployer, account1, account2, account3, ...others] = await ethers.getSigners()
    const utils = ethers.utils
    const Nft = await ethers.getContractFactory('FrenchToastFlavors')
    const nft = await Nft.connect(tokenDeployer).deploy({
      gasLimit: '20000000'
    })


    const Token = await ethers.getContractFactory('FrenchToastken')
    const token = await Token.connect(tokenDeployer).deploy({
      gasLimit: '20000000'
    })

    await nft.deployed()
    await token.deployed()

    const tx = await nft.connect(tokenDeployer).safeMint(auctioneer.getAddress())

    tx.wait()

    await token.connect(tokenDeployer).transfer(account1.getAddress(), utils.parseEther('10000'))
    await token.connect(tokenDeployer).transfer(account2.getAddress(), utils.parseEther('10000'))

    const Auction = await ethers.getContractFactory('Auction')
    const auction = await Auction.deploy(nft.address, 0, token.address, 1)

    await auction.deployed()

  console.log(
    `Auction deployed to: ${auction.address}`
  );

  
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
