# Auction Project
Yes, the UI doesn't look sharp. Don't worry, I can definitely make great uis, I just didn't for the sake of this project.

### To run
1. `npm install`
2. `npm run start` : this will open port 3006 when completed
3. Connect to the accounts provided in the below section with metamask
4. `npx hardhat node`
5. `npx hardhat console --network localhost`
4. Go to the create auction page and click create auction (must be connected to an account). 
5. There will be a bunch of prompts. This is because we have to deploy the nft, the ERC20 currency, and divvy everything out to users for testing. If nothing is happening and the screen hasn't updated, double check metamask doesn't have more pending transactions. 

### To Use
After deploying, the deployer must authorize the contract (so it can transfer upon auction end) to start the auction. You can then switch to one of the other 4 accounts to bid. 

Once you're done bidding, rescinding, updating, etc., run the following commands INSIDE THE HARDHAT CONSOLE to reach the end of the auction:
```
await network.provider.send('evm_increaseTime', [60*60*12])
await network.provider.send("evm_mine")
```

Then you can log back in as the owner and end the auction. This will transfer the NFT to the buyer, transfer FrenchToastken to the auctioneer, and make the contract unusable.

#### Hardhat provided Private keys
The following are the private keys for the 4 accounts we give some french toastken to for bidding. You can add them to metamask to test the app.
1. 0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80
2. 0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d
3. 0x5de4111afa1a4b94908f83103eb1f1706367c2e68ca870fc3fb9a804cdab365a
4. 0x7c852118294e51e653712a81e05800f419141751be58f605c371e15141b007a6 

### Slither

+--------------------+-------------+---------------+--------------------+--------------+--------------------+
|        Name        | # functions |      ERCS     |     ERC20 info     | Complex code |      Features      |
+--------------------+-------------+---------------+--------------------+--------------+--------------------+
|      Auction       |      16     |               |                    |      No      |    Receive ETH     |
|                    |             |               |                    |              | Tokens interaction |
| FrenchToastFlavors |      53     | ERC165,ERC721 |                    |      No      |      Assembly      |
|   FrenchToastken   |      40     |     ERC20     |     ∞ Minting      |      No      |                    |
|                    |             |               | Approve Race Cond. |              |                    |
|                    |             |               |                    |              |                    |
+--------------------+-------------+---------------+--------------------+--------------+--------------------+

### Coverage

File          |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
--------------|----------|----------|----------|----------|----------------|
 contracts\   |    85.71 |       70 |     87.5 |    94.55 |                |
  Auction.sol |    86.27 |       70 |    91.67 |       96 |        118,121 |
  nft.sol     |      100 |      100 |      100 |      100 |                |
  token.sol   |       50 |      100 |       50 |       50 |             14 |
--------------|----------|----------|----------|----------|----------------|
All files     |    85.71 |       70 |     87.5 |    94.55 |                |
--------------|----------|----------|----------|----------|----------------|

### Commands
```shell
  currencydetails       gets the details of the currency
  highestbid            get the bid of the highest bidder
  highestbidder         get the address of the highest bidder
  secondsleft           gets the number of seconds left in the auction
  tokenaddress          gets the contract address for the auctioned token
  tokenid               gets the token id for sale
```

### Transaction hash
`0xda17a8022c1a5f3a18c1a55a27228a81d2aaf747a06dde8db9786dec1cd9f412`

### Contract verification
[here](https://goerli.etherscan.io/address/0x1dd56543357b5f2cd1cf9c6371049a4d31bb8bae#code)