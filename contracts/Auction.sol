// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import '@openzeppelin/contracts/token/ERC721/IERC721.sol';
import '@openzeppelin/contracts/token/ERC20/IERC20.sol';
import '@openzeppelin/contracts/access/Ownable.sol';
import "@openzeppelin/contracts/utils/Counters.sol";

/// @title An auction contract for nfts
/// @author Nathan Bogan
contract Auction is Ownable {
    using Counters for Counters.Counter;

    uint256 public timeOfAuction;

    uint256 public auctionEndTime;
    bool auctionFinalized;
    uint256 public auctionLengthInHours;

    IERC721 public nftContract;
    uint256 public tokenId;

    IERC20 public biddingCurrency;

    mapping(address => uint256) public bidAmounts; 
    mapping(uint256 => address) public bids;

    bool auctionDied;

    Counters.Counter private bidCounter;

    modifier ownerOwnsNft() {
        if(nftContract.ownerOf(tokenId) != owner()) {
            auctionDied = true;
            require(false, 'contract has been nullified; owner no longer owns nft.');
        }
        _;
    }

    modifier hasLeCash(uint256 bidAmount) {
        require(biddingCurrency.balanceOf(msg.sender) >= bidAmount, 'you dont have enough token');
        require(biddingCurrency.allowance(msg.sender, address(this)) >= bidAmount, 
            'not approved for this bid, you must approve the contract with enough token to transfer your cash');
        _;
    }

    modifier auctionDidntDie() {
        require(!auctionDied, 'whoa, auction died for some reason! maybe the owner transferred his nft away ');
        _;
    }

    modifier auctionIsOngoing() {
        require(timeOfAuction > 0 && block.timestamp < auctionEndTime, 'Auction is not in progress');
        _;
    }

    //Events
    event auctionStarted(uint256 timeOfAuction, uint256 auctionLengthInHours);
    event madeBid(address bidder, uint256 bidAmount);
    event withdrewBid(address bidder);
    event auctionFinal(address buyer, uint256 tokensSpent);
    event auctionNoSale();

    /// @dev setup contract with nft Contract address, tokenId, currency of choice and length of auction
    constructor(IERC721 _nftContract, uint256 _tokenId, IERC20 _biddingCurrency, uint256 _auctionLengthInHours) {
        require(_nftContract.ownerOf(_tokenId) == msg.sender, 'cant sell other peoples stuff brah');
        nftContract = _nftContract;
        tokenId = _tokenId;
        biddingCurrency = _biddingCurrency;
        auctionLengthInHours = _auctionLengthInHours;

        bidCounter.increment();
     }

    /// @dev start auction if nothing has gone wrong
    function startAuction() ownerOwnsNft onlyOwner auctionDidntDie external { 
        require(auctionEndTime == 0, 'need a new contract');
        require(nftContract.getApproved(tokenId) == address(this), 
            'you must approve the contract to have nft access to start the auction');
        timeOfAuction = block.timestamp;
        auctionEndTime = block.timestamp + auctionLengthInHours * 1 hours;
        emit auctionStarted(timeOfAuction, auctionLengthInHours);
     }

    /// @dev update bid for sender
    function bid(uint256 bidAmount) public payable auctionIsOngoing hasLeCash(bidAmount) auctionDidntDie ownerOwnsNft {
        uint256 highestBid = getHighestBidAmount();
        require(bidAmount > highestBid, 'too low, check the highest bid before bidding again');

        bidAmounts[msg.sender] = bidAmount;
        bids[bidCounter.current()] = msg.sender;
        bidCounter.increment();

        emit madeBid(msg.sender, bidAmount);
     }

    /// @dev reset sender bid to 0
     function withdrawBid() public ownerOwnsNft auctionDidntDie {
         require(bidAmounts[msg.sender] > 0, 'you dont have anything to withdraw');
         bidAmounts[msg.sender] = 0;
         
         emit withdrewBid(msg.sender);
     }

    /// @dev if the allotted time has passed, handle transaction for winner
    function finalizeAuction() public onlyOwner ownerOwnsNft auctionDidntDie {
        require(block.timestamp >= timeOfAuction + auctionLengthInHours * 1 hours, 'not over yet stoopid');

        require(biddingCurrency.balanceOf(getHighestBidder()) >= getHighestBidAmount(), 'buyer hasnt enough token');
        require(biddingCurrency.allowance(getHighestBidder(), 
            address(this)) >= getHighestBidAmount(), 'buyer hasnt approved enough token');

        if (getHighestBidAmount() == 0) { //sad day :(
            emit auctionNoSale();
            return; 
        }

        biddingCurrency.transferFrom(getHighestBidder(), owner(), this.getHighestBidAmount());
        nftContract.safeTransferFrom(owner(), getHighestBidder(), tokenId);

        emit auctionFinal(getHighestBidder(), this.getHighestBidAmount());
    }

    /// @dev get details on what phase the auction is in
    function getAuctionStatus() public view returns (string memory message) {
        if (auctionDied) message = 'auction died for some reason sorry';
        else if (timeOfAuction == 0) message = 'hold your horses';
        else if (timeOfAuction > 0 && block.timestamp < auctionEndTime) {
            message = ('it is on like donkey kong right now, bid your life savings away');
        }
        else message = 'too late scrub';
    }

    /// @dev get highest bid
    function getHighestBidAmount() public view returns (uint256) {
        for (uint256 i = bidCounter.current(); i > 0; i--) {
            if (bidAmounts[bids[i]] > 0) {
                return bidAmounts[bids[i]];
            }
        }

        return 0; // no bids or all withdrawn
    }

    /// @dev get highest bidder
    function getHighestBidder() public view returns (address) {
        for (uint256 i = bidCounter.current(); i > 0; i--) {
            if (bidAmounts[bids[i]] > 0) {
                return bids[i];
            }
        }

        return address(0); // no bids or all withdrawn
    }
}