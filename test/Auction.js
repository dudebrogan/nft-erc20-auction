const { expect } = require('chai')
const hardhat = require('hardhat')
const ethers = hardhat.ethers

describe('Auction', async () => {
  const zeroes = '000000000000000000'
  let Auction, auction, Nft, nft, Token, token, auctioneer, tokenDeployer, hasToken1, hasToken2, hasToken3, hasUnapprovedToken, noToken, others,auctionSigner

  const resetAuction = async() => {
    [auctioneer, tokenDeployer, hasToken1, hasToken2, hasToken3, hasUnapprovedToken, noToken, ...others] = await ethers.getSigners()

      Nft = await ethers.getContractFactory('FrenchToastFlavors')
      nft = await Nft.connect(tokenDeployer).deploy()

      Token = await ethers.getContractFactory('FrenchToastken')
      token = await Token.connect(tokenDeployer).deploy()

      await nft.deployed()
      await token.deployed()
      
      await token.connect(tokenDeployer).transfer(hasToken1.getAddress(), 1000)
      await token.connect(tokenDeployer).transfer(hasToken2.getAddress(), 1000)
      await token.connect(tokenDeployer).transfer(hasUnapprovedToken.getAddress(), 1000)

      await nft.connect(tokenDeployer).safeMint(auctioneer.getAddress())

      Auction = await ethers.getContractFactory('Auction')
      auction = await Auction.deploy(nft.address, 0, token.address, 12)

      await auction.deployed()

      await token.connect(hasToken1).approve(auction.address, 1000000000000000)
      await token.connect(hasToken2).approve(auction.address, 1000000000000000)

      await nft.connect(auctioneer).approve(auction.address, 0)

      await hardhat.network.provider.request({
        method: 'hardhat_impersonateAccount', 
        params: [auction.address]
      })

      auctionSigner = await ethers.getSigner(auction.address)
  }
  beforeEach(async() => {
      await resetAuction();
  })

  describe('deployment', async() => {

    it('should not deployed with wrong inputs', async() => {
      const BadAuction = await ethers.getContractFactory('Auction')
      await expect(BadAuction.deploy("0x00", 0, token.connect(tokenDeployer).address, 12)).to.be.rejectedWith(Error)
    })

    it('should have deployed correctly with expected values', async() => {
      expect(await auction.nftContract()).to.have.lengthOf(42)
      expect(await auction.tokenId()).to.be.equal(0)
      expect(await auction.auctionLengthInHours()).to.be.equal(12)
    })
  })

  describe('Starting le auction', async() => {
    it('should start if all gucci', async() => {
      await expect(auction.startAuction())

      const blockNumBefore = await ethers.provider.getBlockNumber();
      const blockBefore = await ethers.provider.getBlock(blockNumBefore);

      expect(await auction.timeOfAuction()).to.not.be.equal(blockBefore.timestamp)
    })

    it('shouldn\'t start if the caller ain\'t the owner', async() => {
      await expect(auction.connect(hasToken1).startAuction()).to.be.revertedWith('Ownable: caller is not the owner')
    })

    it('shouldn\'t start if the owner no longer owns nft', async() => {
      await nft.connect(auctioneer)["safeTransferFrom(address,address,uint256)"](auctioneer.getAddress(), hasToken1.getAddress(), 0)
      await expect(auction.startAuction()).to.be.revertedWith('contract has been nullified; owner no longer owns nft.')
    })

    it('shouldn\'t start contract doesn\'t have approval for the nft', async() => {
      const BadAuction = await ethers.getContractFactory('Auction')
      const badAuction = await BadAuction.deploy(nft.address, 0, token.address, 12)

      await badAuction.deployed()

      await expect(badAuction.startAuction()).to.be.revertedWith('you must approve the contract to have nft access to start the auction')
    })

    it('shouldn\'t let someone bid without auction having started yet', async() => {
      await expect(auction.connect(hasToken1).bid(100)).to.be.revertedWith('Auction is not in progress')
    })

    it('should emit on start', async() => {
      const EmitStartAuction = await ethers.getContractFactory('Auction')
      const emitStartAuction = await EmitStartAuction.deploy(nft.address, 0, token.address, 12);
      await nft.connect(auctioneer).approve(emitStartAuction.address, 0)

      await expect(emitStartAuction.startAuction()).to.emit(emitStartAuction, 'auctionStarted')
    })
  })

  describe('Bidding', async() => {
    it('shouldn\'t let someone bid without auction having started yet', async() => {
      await expect(auction.connect(hasToken1).bid(100)).to.be.revertedWith('Auction is not in progress')
    })

    describe('Bidding with working auction', async() => {
      beforeEach(async() => {
        await resetAuction()
        await expect(auction.startAuction())
      })

      it('should let someone bid once started', async() => {
        await auction.connect(hasToken1).bid(100)
        expect(await auction.bids(1)).to.be.equal(await hasToken1.getAddress())
        expect(await auction.bidAmounts(await hasToken1.getAddress())).to.be.equal(100)
      })

      it('should emit on bid', async() => {
        await expect(auction.connect(hasToken1).bid(80)).to.emit(auction, 'madeBid').withArgs(await hasToken1.getAddress(), 80)
      })

      it('should change high bid on withdraw bid', async() => {
        await auction.connect(hasToken1).bid(100)
        await auction.connect(hasToken2).bid(101)

        expect(await auction.getHighestBidAmount()).to.be.equal(101)
        expect(await auction.getHighestBidder()).to.be.equal(await hasToken2.getAddress())

        await auction.connect(hasToken2).withdrawBid()

        expect(await auction.getHighestBidAmount()).to.be.equal(100)
        expect(await auction.getHighestBidder()).to.be.equal(await hasToken1.getAddress())
      })
      
      it('should emit on withdraw bid', async() => {
        await auction.connect(hasToken1).bid(100)
        await expect(auction.connect(hasToken1).withdrawBid()).to.emit(auction, 'withdrewBid').withArgs(await hasToken1.getAddress())
      })

      it('shouldn\'t let someone bid without contract having been approved for erc20', async() => {
        await expect(auction.connect(hasUnapprovedToken).bid(100)).to.be
          .revertedWith('not approved for this bid, you must approve the contract with enough token to transfer your cash')
      })

      it('shouldn\'t let users without enough token bid', async() => {
        await expect(auction.connect(hasToken2).bid(1001)).to.be.revertedWith('you dont have enough token')
      })

      it('should update max bid after new highest bid', async() => {
        await auction.connect(hasToken1).bid(100)
        expect(await auction.getHighestBidAmount()).to.be.equal(100)
        expect(await auction.getHighestBidder()).to.be.equal(await hasToken1.getAddress())

        await auction.connect(hasToken2).bid(101)
        expect(await auction.getHighestBidAmount()).to.be.equal(101)
        expect(await auction.getHighestBidder()).to.be.equal(await hasToken2.getAddress())
      })

      it('should not allow bid if post auction', async() => {
        const blockNumBefore = await ethers.provider.getBlockNumber();
        const blockBefore = await ethers.provider.getBlock(blockNumBefore); // Want your mind blown? comment this UNUSED line out and the below expect will break.

        await ethers.provider.send('evm_increaseTime', [60*60*12000])
        await expect(auction.connect(hasToken2).bid(102)).to.be.revertedWith('Auction is not in progress')
      })
    })
  })

  describe('Ending the contract', async() => {

    beforeEach(async() => {
      await expect(auction.startAuction())
      await auction.connect(hasToken1).bid(100)
      await auction.connect(hasToken2).bid(101)
    })
    it('should only be callable by owner', async() => {
      await expect(auction.connect(hasToken1).finalizeAuction()).to.be.revertedWith('Ownable: caller is not the owner')
    })
    it('should revert before time has passed', async() => {
      await expect(auction.finalizeAuction()).to.be.revertedWith('not over yet stoopid')
    })
    
    describe('ready to finalize contract', async() => {
      beforeEach(async() => {
        await ethers.provider.send('evm_increaseTime', [60*60*12000])
      })

      it('should have transferred the goods', async() => {
        await auction.finalizeAuction()
        expect(await token.balanceOf(auctioneer.getAddress())).to.be.equal(101)
        expect(await nft.ownerOf(0)).to.be.equal(await hasToken2.getAddress())
      })

      it('should fail if owner doesnt have nft', async() => {
        await nft.connect(auctioneer)["safeTransferFrom(address,address,uint256)"](auctioneer.getAddress(), hasToken1.getAddress(), 0)
        await expect(auction.finalizeAuction()).to.be.revertedWith('contract has been nullified; owner no longer owns nft.')
      })

      it('should fail if buyer hasnt enough token', async() => {

        await token.connect(hasToken2).transfer(hasToken1.getAddress(), 1000)
        await expect(auction.finalizeAuction()).to.be.revertedWith('buyer hasnt enough token')
      })

      it('should fail if contract isn\'t approved anymore', async() => {
        await token.connect(hasToken2).approve(auction.address, 0)
        await expect(auction.finalizeAuction()).to.be.revertedWith('buyer hasnt approved enough token')
      })

      it('should emit event when everything goes correctly', async() => {
        await expect(auction.finalizeAuction()).to.emit(auction, 'auctionFinal').withArgs(await hasToken2.getAddress(), 101)
      })

      it('should emit event when no one buys', async() => {
        const EmitAuction = await ethers.getContractFactory('Auction')
        const emitAuction = await EmitAuction.deploy(nft.address, 0, token.address, 12);
        await nft.connect(auctioneer).approve(emitAuction.address, 0)
        await ethers.provider.send('evm_increaseTime', [60*60*12000])

        await expect(emitAuction.finalizeAuction()).to.emit(emitAuction, 'auctionNoSale').withArgs()
      })
    })
  })
});
