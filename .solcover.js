module.exports = {
    host: "localhost",
    network_id: "*",
    port: 8555,         
    gas: 0xfffffffffff,
    gasPrice: 0x01
};