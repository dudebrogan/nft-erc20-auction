const { task } = require('hardhat/config')

require('@nomicfoundation/hardhat-toolbox')
require('@nomiclabs/hardhat-web3')
require('dotenv').config()
require('hardhat-docgen');

const { AUCTION_CONTRACT } = process.env
/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: '0.8.14',
  networks: {
    hardhat: {
      chainId: 1337,
    }
  },
  hardhat: {
    mining: {
      auto: true,
      interval: 5000
    }
  },
  docgen: {
    path: './docs',
    clear: true,
    runOnCompile: true
  }
}

task('highestbidder', 'get the address of the highest bidder')
  .setAction(async() => {
    const Auction = await ethers.getContractFactory('Auction')
    const auction = await Auction.attach(AUCTION_CONTRACT)

    const bidder = await auction.getHighestBidder()

    console.log(`highest bidder is ${bidder}`)
  })

task('highestbid', 'get the bid of the highest bidder')
  .setAction(async() => {
    const Auction = await ethers.getContractFactory('Auction')
    const auction = await Auction.attach(AUCTION_CONTRACT)

    const bal = await auction.getHighestBidAmount()

    console.log(`highest balance is ${bal}`)
  })  

task('tokenid', 'gets the token id for sale')
  .setAction(async() => {
    const Auction = await ethers.getContractFactory('Auction')
    const auction = await Auction.attach(AUCTION_CONTRACT)

    const tokenId = await auction.tokenId()

    console.log(`token id is: ${tokenId}`)
  })

task('tokenaddress', 'gets the contract address for the auctioned token')
  .setAction(async() => {
    const Auction = await ethers.getContractFactory('Auction')
    const auction = await Auction.attach(AUCTION_CONTRACT)

    const nftContract = await auction.nftContract()

    console.log(`nftContract id is: ${nftContract}`)
  })

task('currencydetails', 'gets the details of the currency')
  .setAction(async() => {
    const Auction = await ethers.getContractFactory('Auction')
    const auction = await Auction.attach(AUCTION_CONTRACT)

    const address = auction.biddingCurrency()

    const ERC20 = await ethers.getContractFactory('ERC20')
    const erc20 = await ERC20.attach(address)

    const name = await erc20.name()

    console.log(`currency address is ${address}`)
    console.log(`the name of the token is: ${name}`)
  })

task('secondsleft', 'gets the number of seconds left in the auction')
  .setAction(async() => {
    const Auction = await ethers.getContractFactory('Auction')
    const auction = await Auction.attach(AUCTION_CONTRACT)
    const auctionEndTime2 = await auction.auctionEndTime()

    const latestBlock = await hre.ethers.provider.getBlock('latest')
    
    console.log(`the number of seconds left is ${(auctionEndTime - latestBlock.timestamp) * 1000 }`)
  })